# 00_main.r
# :purpose: Run clustering on ta-feng dataset for demo
# :author: jon.sedar@applied.ai
# :date: Thu 03 Oct 2013
# :note: Roughly 700 LOC in all scripts


### header ---------------------------------------------------------------------
require(stringr)

### locations
proj <- str_c("~/Documents/workspace/clustering/")
dirs <- c(str_c(proj,"code/"),str_c(proj,"data/"),str_c(proj,"img/"))
names(dirs) <- c("code","data","img")

### sister files and libraries
source(str_c(dirs['code'],'js_standard_functions.r'))
l <- libGetter(c('data.table','Hmisc','ggplot2','scales','grid'
                 ,'reshape2','lubridate','mclust','mix','zoo'))


### 01 load data ---------------------------------------------------------------
source(str_c(dirs['code'],'01_sourcing.r'))
dt <- get_data()


### 02 basic checks, final cleaning, viz ---------------------------------------
source(str_c(dirs['code'],'02_exploration.r'))


### 03 feature creation --------------------------------------------------------
source(str_c(dirs['code'],'03_featurecreation.r'))


### 00 pitstop to load / reload data post feature creation ---------------------
cst <- data_loader(str_c(dirs['data'],"cst.rds"),loadraw=F)
write.csv(cst,str_c(dirs['data'],'cst.csv'),row.names=F,quote=F)


### 04 feature selection -------------------------------------------------------
source(str_c(dirs['code'],'04_featureselection.r'))



### 05 Run EM with GMM ---------------------------------------------------------
source(str_c(dirs['code'],'05_clustering.r'))


