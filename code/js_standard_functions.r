# ==============================================================================
#    js_standard_functions.r
#    :purpose: Maintain a library of useful general utilities for R scripting
#    :author: Jon Sedar
#    :date: Sun 18 Aug 2013
# ==============================================================================

posix2isotxt <- function(dateobj){format(dateobj,"%Y-%m-%d")}
posix2ctxt <- function(dateobj){str_replace_all(as.character(dateobj),"-","")}
posix2dbtxt <- function(dateobj){toupper(format(dateobj,"%d-%b-%y"))}
rounddec <- function(x, k=2){as.numeric(format(round(x, k), nsmall=k))}
str_rounddec <- function(x, k=2){format(round(x, k), nsmall=k)}
prct <- function(x,k=1){str_c(as.character(str_rounddec(x*100,k)),"%")}
to_numeric <- function(x){as.numeric(as.character(x))}


libGetter <- function(y){
    r <- 'libGetter'
    for(x in y){
        r <- c(r,paste(x,'loaded correctly'))
        if(!require(x,character.only=TRUE,quietly=TRUE,warn.conflicts=FALSE)){
            install.packages(x, repos="http://cran.ma.imperial.ac.uk/")
            if(require(x,character.only=TRUE,quietly=TRUE,warn.conflicts=FALSE)){
                r <- c(r,paste(x,'installed and loaded'))
            } else {
                r <- c(r,paste('could not install',x))
            }
        }
    }
    return(r)
}



### Local functions
get_zerocols <- function(x){
    ### accept data.frame, return char vector of colnames that contain only zeros
    y <- sapply(x, function(xx){all(xx==0)})
    names(y[y])
}

get_nacols <- function(x){
    ### accept data.frame, return char vector of colnames that contain NAs
    ### nabbed from http://stackoverflow.com/questions/10574061/show-columns-with-nas-in-a-data-frame
    y <- sapply(x, function(xx){any(is.na(xx))})
    names(y[y])
}

get_circle <- function(center=c(0,0), npoints=100){
    ### create a data.table of a circle
    r<-1
    tt<-seq(0,2*pi,length=npoints)
    xx<-center[1]+r*cos(tt)
    yy<-center[1]+r*sin(tt)
    return(data.table(x=xx,y=yy))
}


xt_modder <- function(xt){
    ## accepts xtable object, returns modified latex string without rownames for cat-ing
    c <- capture.output(print.xtable(xt,include.rownames=FALSE))
    mod <- str_replace(c,"\\\\begin\\{table\\}\\[ht\\]","\\\\begin\\{table\\}\\[H\\]")
    mod <- str_replace(mod, "\\\\begin\\{tabular\\}", "\\\\scriptsize\\\\ttfamily\\\\begin\\{tabular\\}")        
    return(mod)
}

str_trimmer <- function(x,len=40) {
    # accepts a df (character cols only), returns a df with long character strings trimmed
    m <- x
    if(!is.numeric(x)) {
        n <- lapply(x,function(y){
                if(nchar(y) <= len){
                    y
                } else {
                    paste(str_sub(y[[1]],1,len-1),'$',sep='')
                }
            }
        )
        m <- as.character(n)
    }
    return(m)
}



data_saver <- function(obj,fdir){
    ### saves a single object to rds using the object's name
    objname <- deparse(substitute(obj))
    saveRDS(obj,file=str_c(fdir,objname,".rds"))
#     flog.info('Saved %s to rds.',objname)
}


data_loader <- function(fqname,cols=c(F),sp="|",hdr=F,skp=0,loadraw=T){
    ### load an object from rds or csv or if exists will assign to new variable
    obj <- NULL
        
    if(loadraw){
        if (cols[[1]][1]!=F){
            obj <- data.table(read.table(fqname,sep=sp,header=hdr,skip=skp
                                        ,col.names=cols$names
                                        ,colClasses=cols$types
                                        ,encoding="UTF-8",stringsAsFactors=F))
        } else {
            obj <- data.table(read.table(fqname,sep=sp,header=hdr,skip=skp
                                        ,encoding="UTF-8",stringsAsFactors=F))
        }
#         flog.info('Loaded %s from text file: ',fqn_raw)
    
    } else if(file.exists(fqname)) {
        obj <- readRDS(fqname)
#         flog.info('Loaded %s from rds',fqname)
                             
    } else {
        return("NOTEXIST")
    }
    return(obj)
}

str_pyformat <- function(string, ...) {
    # Replicate functionality of Python's string.format()
    i <- 0
    for( arg in list(...) ) {
        to_replace <- paste( sep='', "\\{", i, "\\}" )
        string <- gsub( to_replace, arg, string )
        i <- i + 1
    }
    return(string)
}

get_random_word <- function(ngramlen,txt="hipster"){
    ### return an n-gram chosen randomly from chosen text
    ngramlen <- 1   ### hack for now
    
    hipster_lorem <- "Keffiyeh postironic actually, art party Portland occupy 
            Wes Anderson Blue Bottle ugh Odd Future. Umami Williamsburg Cosby 
            sweater aesthetic. XOXO dreamcatcher cray whatever asymmetrical. 
            Cred Godard Pinterest, you probably havent heard of them direct 
            trade Carles organic pourover banh mi forage distillery. PBR 
            artisan retro before they sold out Neutra tousled, occupy skateboard
            Thundercats Odd Future seitan biodiesel. Slowcarb aesthetic next 
            level 8bit. Leggings squid, mustache chia gentrify Vice."
    
    lm <- ""
    if(txt=="hipster"){
        lm <- hipster_lorem
    }
    
    lm_nopunct <- str_replace_all(lm, '[[:punct:]]',' ')
    lm_onlyspace <- str_trim(str_replace_all(str_replace_all(lm_nopunct,'\n',''),'\\s+',' '))
    lm_vec <- unlist(str_split(lm_onlyspace, " "))
    
    ngram <- lm_vec[sample(seq(1,length(lm_vec),1),1)]
    
    return(ngram)   
}


### colors
colorComp <- c('')
colorShade <- c('')
colorTri <- c('')
colorTet <- c('')
colorAna <- c('')
colorBlind <- c("#666666","#E69F00","#56B4E9","#009E73","#F0E442","#0072B2","#D55E00","#CC79A7")
